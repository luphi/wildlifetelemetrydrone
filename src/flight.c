/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "flight.h"

void calculate_and_set_flight_path() {
	// Waypoints are written to the Pixhawk (and other MAVLink devices) like so:
	// 1) Send waypoint count, indicating the number of waypoints to set
	// 2) The Pixhawk sends a waypoint request for waypoint 0
	// 3) Waypoint 0 is sent to the Pixhawk
	// 4) The Pixhawk sends a waypoint request for waypoint 1
	// 5) Waypoint 1 is sent to the Pixhawk
	// 6) Repeat <count> times
	// Waypoints may be actual waypoints or navigation commands such as loiter.
	// Because we must respond at the time of a request, the waypoints are sent
	// from mavlink.c's mavlink_process_receive() but are assigned in this
	// function via global variables.
	// Waypoint 0 and waypout <count> should be takeoff and land, respectively.
	// 0) Takeoff
	// 1) First waypoint
	// 2) Loiter 1 turn
	// 3) Second waypoint
	// 4) Loiter 1 turn
	// 5) Third waypoint
	// 6) Loiter 1 turn
	// 7) Return to launch
	// 8) Land
	// 9 total navigation commands so the count to send is 9.
	// Waypoints are sent as a MISSION_ITEM.  Our goal will be to send local
	// coordinates, meaning meters to travel in (x, y) coordinates, as opposed
	// to GPS lat/lon as converting to and from WGS84 is annoying.
	
	// note: MAV_FRAME_LOCAL_ENU is not supported by the Pixhawk
	
	mavlink_message_t mission_item; // temp variable to be assigned
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 0, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_TAKEOFF, 1, 1, 0.0f,
		0.0f, 2.0f, 1.0f, 0.0f, 0.0f, 1.0f /* altitude target in meters (?) */);
	waypoints[0] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 1, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_WAYPOINT, 0, 1,
		0.0f /* hold time, 0 because the next waypoint is loiter */, 1.0f /* 1 m
		of radius error */, 0.0f, 0.0f, 35.174871f /* WGS84 latitude*/,
		-111.656678f /* WGS84 longitude */, 1.0f /* relative altitude */);
	waypoints[1] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 2, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_LOITER_TURNS, 0,
		1, 1.0f /* loiter for 1 turn */, 0.0f, 1.0f /* 1 m of radius error */,
		0.0f, 35.174871f /* WGS84 latitude*/, -111.656678f /* WGS84 longitude */, 1.0f);
	waypoints[2] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 3, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_WAYPOINT, 0, 1,
		0.0f /* hold time, 0 because the next waypoint is loiter */, 1.0f /* 1 m
		of radius error */, 0.0f, 0.0f, 35.174765f /* WGS84 latitude */,
		-111.656813f /* WGS84 longitude */, 1.0f /* relative altitude */);
	waypoints[3] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 4, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_LOITER_TURNS, 0,
		1, 1.0f /* loiter for 1 turn */, 0.0f, 1.0f /* 1 m of radius error */,
		0.0f, 35.174765f /* WGS84 latitude */,
		-111.656813f /* WGS84 longitude */, 1.0f /* relative altitude */);
	waypoints[4] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 5, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_WAYPOINT, 0, 1,
		0.0f /* hold time, 0 because the next waypoint is loiter */, 1.0f /* 1 m
		of radius error */, 0.0f, 0.0f, 35.174769f /* WGS84 latitude*/,
		-111.656583f /* WGS84 longitude */, 1.0f /* relative altitude */);
	waypoints[5] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 6, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_LOITER_TURNS, 0,
		1, 1.0f /* loiter for 1 turn */, 0.0f, 1.0f /* 1 m of radius error */,
		0.0f, 35.174769f /* WGS84 latitude*/,
		-111.656583f /* WGS84 longitude */, 1.0f /* relative altitude */);
	waypoints[6] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 7, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_RETURN_TO_LAUNCH,
		0, 1, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f); // params irrelevant
	waypoints[7] = mission_item;
	mavlink_msg_mission_item_pack(255, 190, &mission_item,
		1, 0, 8, MAV_FRAME_GLOBAL_RELATIVE_ALT, MAV_CMD_NAV_LAND, 0, 1, 0.0f,
		0.0f, 0.0f, 0.0f, 35.174871f /* WGS84 latitude*/,
		-111.656678f /* WGS84 longitude */, 1.0f /* relative altitude */);
	waypoints[8] = mission_item;
	
	mavlink_msg_mission_count_pack(system_id, component_id, &mavlink_message,
		1, 0, 9);
	mavlink_send(mavlink_message);
	// At this point, mavlink_process_receive() will send the calculated
	// waypoints as requested
}

void start_flight() {
	mavlink_msg_command_long_pack(system_id, component_id, &mavlink_message,
		target_system, 0, MAV_CMD_MISSION_START,
		0 /* confirmation */, 0 /* param 1 */, 8 /* param 2 */, 0, 0, 0, 0, 0);
	mavlink_send(mavlink_message);
}

