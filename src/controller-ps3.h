/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <linux/joystick.h> // joystick API, what else?

#define JSDEVICE "/dev/input/js0" // "/dev" = devices, "js0" = joystick 0

#define SELECT_BUTTON         0
#define L3_BUTTON             1
#define R3_BUTTON             2
#define START_BUTTON          3
#define DPAD_UP               4
#define DPAD_RIGHT            5
#define DPAD_DOWN             6
#define DPAD_LEFT             7
#define L2_BUTTON             8
#define R2_BUTTON             9
#define L1_BUTTON             10
#define R1_BUTTON             11
#define TRIANGLE_BUTTON       12
#define CIRCLE_BUTTON         13
#define X_BUTTON              14
#define SQUARE_BUTTON         15
#define PS_BUTTON             16

struct js_event event; // holds information on movements and buttons
int controller; // the handle to the device
unsigned char axes; // number of axes/joysticks on the device
unsigned char buttons; // number of buttons on the device
int version; // driver version
char name[128]; // name of the device
int init_presses[19]; // array of boolean-ish values indicating init presses
int button_down[19]; // 1 if button is currently down, eliminates duplicates

/* 
 * Open the handle to the device and determine the number of axes and
 * buttons as well as the driver version and device name.
 * 
 * If successful, the function will return 0.  Otherwise, it has failed.
 */
int init_joystick();

/*
 * Handle joystick events and respond to them, specifically for the PS3
 * controller.  Although it may be adapted for other devices, all events assume
 * values based on a PS3 controller.
 */
int controller_receive();

#endif
 
