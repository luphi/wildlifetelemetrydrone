/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "audio.h"

/*
 * Initialize handles and otherwise get the program ready to read from the
 * chipset buffer.
 */
int init_audio() {
	sprintf(device_name, "default");
	channels = 1;
	rate = 44100;
	stream = SND_PCM_STREAM_CAPTURE;
	acc = SND_PCM_ACCESS_RW_INTERLEAVED;
	format = SND_PCM_FORMAT_S16_LE;
	audio_bytes = 0;

	snd_pcm_hw_params_t* hw_params; // temporarily holds hardware parameters
	snd_pcm_sw_params_t* sw_params; // temporarily holds software parameters
	int res; // RESult of functions, return value
	
	res = snd_pcm_open(&alsa_handle, device_name, stream, 0);
	if (res < 0) {
		fprintf(stderr, "Failed to open handle to \"default\" (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_nonblock(alsa_handle, 1);
	if (res < 0) {
		fprintf(stderr, "Couldn't set audio handle to nonblock (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_malloc(&hw_params);
	if (res < 0) {
		fprintf(stderr, "Failed to allocate hardware parameters structure "
			"(%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_sw_params_malloc(&sw_params);
	if (res < 0) {
		fprintf(stderr, "Failed to allocate software parameters structure "
			"(%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_any(alsa_handle, hw_params);
	if (res < 0) {
		fprintf(stderr, "Couldn't initialize hardware parameters structure "
			"(%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_access(alsa_handle, hw_params, acc);
	if (res < 0) {
		fprintf(stderr, "Couldn't set audio access type (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_format(alsa_handle, hw_params, format);
	if (res < 0) {
		fprintf(stderr, "Couldn't set PCM format type (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_channels(alsa_handle, hw_params, channels);
	if (res < 0) {
		fprintf(stderr, "Couldn't set number of channels to %d (%s)\n",
			channels, snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_rate_near(alsa_handle, hw_params, &rate, 0);
	if (res < 0) {
		fprintf(stderr, "Couldn't set rate near %d (%s)\n", rate,
			snd_strerror(res));
		return -1;
	}
	
	unsigned int period_time = 0;
	unsigned int buffer_time = 0;
	snd_pcm_uframes_t period_frames = 0;
	snd_pcm_uframes_t buffer_frames = 0;
	snd_pcm_uframes_t buffer_size = 8192; // increasing this more will not help
	snd_pcm_uframes_t chunk_size;
	
	res = snd_pcm_hw_params_get_buffer_time_max(hw_params, &buffer_time, 0);
	if (res < 0)
		return -1;
	if (buffer_time > 500000)
		buffer_time = 500000;
	
	if (buffer_time > 0)
		period_time = buffer_time / 4;
	else
		period_frames = buffer_frames / 4;
	
	if (period_time > 0)
		res = snd_pcm_hw_params_set_period_time_near(alsa_handle, hw_params,
			&period_time, 0);
	else
		res = snd_pcm_hw_params_set_period_size_near(alsa_handle, hw_params,
			&period_frames, 0);
	if (res < 0)
		return -1;
	
	if (buffer_time > 0)
		res = snd_pcm_hw_params_set_buffer_time_near(alsa_handle, hw_params,
			&buffer_time, 0);
	else
		res = snd_pcm_hw_params_set_buffer_size_near(alsa_handle, hw_params,
			&buffer_size);
	if (res < 0)
		return -1;
	
	res = snd_pcm_hw_params(alsa_handle, hw_params);
	if (res < 0) {
		fprintf(stderr, "Failed to set audio hardware parameters (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	snd_pcm_hw_params_get_period_size(hw_params, &chunk_size, 0);
	snd_pcm_hw_params_get_buffer_size(hw_params, &buffer_size);
	if (buffer_size == chunk_size) {
		fprintf(stderr, "Chunk size and buffer size must differ\n");
		return -1;
	}
	
	snd_pcm_sw_params_current(alsa_handle, sw_params);
	
	res = snd_pcm_sw_params_set_avail_min(alsa_handle, sw_params, chunk_size);
	if (res < 0) {
		fprintf(stderr, "Couldn't set available minimum frames to consider a "
			"PCM stream ready (%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_sw_params_set_stop_threshold(alsa_handle, sw_params,
		buffer_size);
	if (res < 0) {
		fprintf(stderr, "Couldn't set audio record stop threshold to buffer "
			"size (%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_sw_params(alsa_handle, sw_params);
	if (res < 0) {
		fprintf(stderr, "Failed to set audio software parameters (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	snd_pcm_hw_params_free(hw_params);
	snd_pcm_sw_params_free(sw_params);
	
	//res = snd_pcm_prepare(alsa_handle);
	//if (res < 0) {
		//fprintf(stderr, "Couldn't prepare audio interface for use (%s)\n",
			//snd_strerror(res));
		//return -1;
	//}
	
	// FRAMES_READ is the number of frames read in the read_audio() function
	alsa_buffer = (unsigned char*) malloc(2 * channels * FRAMES_READ);

	return 0;
}

/*
 * Writes WAV header information such that decoders can play it properly.
 * This function is dependent on a couple variables (channels and rate) and will
 * adapt accordingly.
 *
 * Starts the read thread as well.
 */
void start_audio() {
	struct wave_header header;

	header.riff[0] = 'R';
	header.riff[1] = 'I';
	header.riff[2] = 'F';
	header.riff[3] = 'F';
	header.file_size = 0; // will be rewritten later
	header.wave[0] = 'W';
	header.wave[1] = 'A';
	header.wave[2] = 'V';
	header.wave[3] = 'E';
	header.fmt[0] = 'f';
	header.fmt[1] = 'm';
	header.fmt[2] = 't';
	header.fmt[3] = ' ';
	header.bit_depth = 16; // 16-bit
	header.format = 1; // 1 is for integer PCM
	header.channels = channels; // number of channels
	header.sample_rate = rate; // sample rate in hertz
	header.bytes_per_second = rate * 16 * channels / 8; // rate * bits per sample * channels / 8
	header.block_align = 2 * channels; // bits per sample * channels / 8
	header.bits_per_sample = 16;
	header.data[0] = 'd';
	header.data[1] = 'a';
	header.data[2] = 't';
	header.data[3] = 'a';
	header.data_size = 0; // will be rewritten later
	
	// A wave header is 44 bytes and we're writing 1
	fwrite(&header, 44, 1, pcm_log);
	
	int p; // unused variable but the thread requires a parameter
	if (pthread_create(&thread, NULL, read_audio, &p)) // create thread
		fprintf(stderr, "Failed to create audio thread - audio is not "
			"recording\n");
}

/*
 * Writes the data/file size information into the header.  Must be called
 * before the file handle is closed.
 */
void stop_audio() {
	// The first chunk data size is given as file size - 8 where file size is
	// data + header.  This information is 4 bytes in size located at 0x0004.
	unsigned int file_size = ((unsigned int) audio_bytes +
		sizeof(struct wave_header) - 8);
	fseek(pcm_log, 4, SEEK_SET); // begin writing at byte 5
	fwrite(&file_size, 4, 1, pcm_log); // write 4 bytes
	// The next data size is the size of the data itself excluding header bytes
	// which is just audio_bytes.  This is located at byte 41 (0x0028)
	unsigned int data_size = ((unsigned int) audio_bytes);
	fseek(pcm_log, 40, SEEK_SET); // begin writing at byte 41
	fwrite(&data_size, 4, 1, pcm_log); // write 4 bytes
}

/*
 * Reads and writes the audio while still logging.  Will run continuously in its
 * own thread.
 *
 * Reads FRAMES_READ frames of audio.  This value may be increased if necessary
 * but the buffer must be increased with it.
 */
//int read_audio() {
void* read_audio(void* v) {
	while (logging) {
		int res;
		res = snd_pcm_readi(alsa_handle, alsa_buffer, FRAMES_READ);
		if (res < 0 && res != -EAGAIN) { // EAGAIN ignored assuming non-blocking
			fprintf(stderr, "Error occurred while reading audio input (%s)\n",
				snd_strerror(res));
			return NULL;
			logging = 0;
		}
		if (res > 0) {
			int bytes = res * channels * 2; // because 16 bits / 8 = 2 bytes
			audio_bytes += bytes;
			fwrite(alsa_buffer, bytes, 1, pcm_log);
		}
	}
	return NULL;
}

