/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MAVLINK_H_
#define _MAVLINK_H_

#include <mavlink.h> // UART serial communication (with pixhawk) library
#include <sys/ioctl.h> // hardware control

#define UARTDEVICE "/dev/ttyACM0" // ACM0 is USB, real UART is also available

int pixhawk; // handle to serial communication
int system_id; // unique (but arbitrary) ID number for this vehicle
int component_id; // refers to a component of a system, mostly useless for us
int target_system;
int target_component;
int system_status; // defined by MAVlink; standby, active, emergency, etc.
char send_buffer[300]; // 300 is an arbitrary size used in MAVlink examples
int message_received; // 0 = nothing received, 1 = something received
int pulse_width;
mavlink_system_t mavlink_system;
mavlink_message_t mavlink_message;

int current_gps_lat;
int current_gps_lon;

/*
 * Initialize UART communication for use with the MAVlink protocol/library.
 * The MAVlink library itself doesn't need to be initialized but all related
 * initialization is handled here.
 *
 * If successful, the function will return 0.  Otherwise, it has failed.
 */
int init_mavlink();

/*
 * Send a MAVlink message over UART.  This is a blocking function, meaning it
 * halts processing until the sending is complete.  It blocks for a very short
 * time by human standards but there's a chance the blocking may prevent, for
 * example, input handling from the controller.
 *
 * In the future, this should probably be threaded.
 */
void mavlink_send(mavlink_message_t message);

/*
 * This is a UART-related function which does little more than check for UART
 * data.  The processing of this data is handled by another function.
 *
 * In the future, this should probably be threaded with O_NONBLOCK disabled.
 */
int mavlink_read();

/*
 * If a message has been received, as determined by mavlink_read(), process the
 * the message accordingly.  This is not a full list of MAVlink messages but
 * is/will be all the ones pertaining to this drone.
 *
 * The message itself is stored in the global variable mavlink_message.
 */
void mavlink_process_receive();

#endif

