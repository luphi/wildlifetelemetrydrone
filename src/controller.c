/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "controller.h"
#include "mavlink.h"
#include "audio.h"
#include "globals.h"
#include "flight.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <wiringPi.h> // for its timing functions

int init_controller() {
	sock_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_desc < 0) {
		fprintf(stderr, "Failed to create socket\n");
		return -1;
	}
	
	bzero((char*) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(6668);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	if (bind(sock_desc, (struct sockaddr*) &serv_addr,
			sizeof(serv_addr)) < 0) {
		fprintf(stderr, "Failed to bind socket\n");
		return -1;
	}
	
	socket_quit = 0;
	socket_closed = 0;
	last_ping = millis();

	int p; // unused variable but the thread requires a parameter
	if (pthread_create(&thread, NULL, socketRun, &p)) { // create thread
		fprintf(stderr, "Failed to create socket thread - Remote "
			"control of the drone is not possible\n");
		return -1;
	}
	
	pulse_width = 1125;
	
	return 0;
}

void close_controller() {
	if (socket_closed)
		return;
	if (debug) printf("Closing socket...\n");
	socket_quit = 1; // may already be 1
	socket_closed = 1;
	close(conn_desc);
	close(sock_desc);
}

int controller_check() {
	int or = socket_closed << 1 | socket_quit;
	switch (or) {
		case 0x0000: // all variables 0
			return 0;
		case 0x0001: // socket open and quit requested
			return 1;
		case 0x0002: // socket closed, nothing to do
			return 2;
		case 0x0003: // socket closed and quit requested
			return 3;
	}
	return 0; // won't happen but keeps the compiler happy
}

void* socketRun(void* p) {
	listen(sock_desc, 100); // blocking function
	if (debug || verbose) printf("Socket is listening...\n");
	socketAccept();
	
	while (!quit) {
		if (millis() - last_ping >= 10000) { // 10 seconds since last ping
			if (debug || verbose) printf("Connection to ground station "
				"lost.  Socket is listening again...\n");
			socket_closed = 1;
			socketAccept();
		}
		
		int res = socketRecv();
		if (res == 0) {
			if (strstr(buffer, "PING") != NULL) {
				char temp[16]; // holds the number of "PING 12345678" (e.g. "12345678")
				char temp2[32]; // holds the whole string to send
				strcpy(temp, buffer + 5); // buffer + 5 is the beginning of the number
				sprintf(temp2, "PONG %s", temp);
				socketSend(temp2);
				last_ping = millis();
			}
			else if (strcmp(buffer, "ARM") == 0) {
				mavlink_msg_command_long_pack(system_id, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL,	MAV_CMD_COMPONENT_ARM_DISARM,
					0 /* confirmation */, 1 /* param 1 */, 0, 0, 0, 0, 0, 0);
				if (!mavlink_disabled) {
					mavlink_send(mavlink_message);
					if (verbose || debug) printf("Sent arming command\n");
				}
				else
					socketSend("DEBUG MAVlink is disabled.  There is no flight "
						"controller to arm.");
			}
			else if (strcmp(buffer, "DISARM") == 0) {
				mavlink_msg_command_long_pack(system_id, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL,	MAV_CMD_COMPONENT_ARM_DISARM,
					0 /* confirmation */, 0 /* param 1 */, 0, 0, 0, 0, 0, 0);
				if (!mavlink_disabled) {
					mavlink_send(mavlink_message);
					if (verbose || debug) printf("Sent disarming command\n");
				}
				else
					socketSend("DEBUG MAVlink is disabled.  There is no flight "
						"controller to disarm.");
			}
			else if (strcmp(buffer, "STARTRECORD") == 0) {
				logging = 1; // must be set before audio starts
				// The "interval" parameter (10 below) is actually 1/n rather
				// than n such that higher numbers produce smaller time
				// intervals (a rate).  The target system is 1.
				if (!mavlink_disabled) {
					mavlink_msg_request_data_stream_pack(system_id,
						component_id, &mavlink_message, 1, 0,
						MAV_DATA_STREAM_POSITION, 10, 1); // 1 to start sending, 0 to stop sending
					mavlink_send(mavlink_message);
				}
				if (!audio_disabled) {
					start_audio(); // write 99% of the WAV header
					logging_start_time = clock();
					if (verbose || debug) printf("Beginning logging\n");
				}
				else {
					socketSend("DEBUG Audio recording is disabled.  Logging is "
						"not possible.");
					logging = 0;
				}
			}
			else if (strcmp(buffer, "STOPRECORD") == 0) {
				logging = 0; // must be set before audio stops
				if (!mavlink_disabled) {
					mavlink_msg_request_data_stream_pack(system_id,
						component_id, &mavlink_message, 1, 0,
						MAV_DATA_STREAM_POSITION, 10, 0); // 0 to stop sending, 1 to start sending
					mavlink_send(mavlink_message);
				}
				if (!audio_disabled) {
					stop_audio(); // finish the WAV header
					if (verbose || debug) printf("Ending logging\n");
				}
				else
					socketSend("DEBUG Audio recording is disabled.  Logging is "
						"not possible.  No logging occurred.");
			}
			else if (strcmp(buffer, "THROTTLELOW") == 0) {
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, 1150, 1150, 1150, 1150,
					1150, 1150, 1150, 1150);
				if (!mavlink_disabled) {
					mavlink_send(mavlink_message);
					pulse_width = 1150;
					if (verbose || debug) printf("Sent RC channel override 1150\n");
				}
				else
					socketSend("DEBUG MAVlink is disabled.  Low throttle "
						"ignored.");
			}
			else if (strcmp(buffer, "THROTTLEHIGH") == 0) {
				// I spent too many hours trying to get this to work
				// only to discover that the Pixhawk only accepts this command
				// from system 255.  System 255 doesn't even exist.
				// Also, the value is the pulse width in microseconds going from
				// something around 1000 to... I don't know
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, 2000, 2000, 2000, 2000,
					2000, 2000, 2000, 2000);
				if (!mavlink_disabled) {
					mavlink_send(mavlink_message);
					pulse_width = 2000;
					if (verbose || debug) printf("Sent RC channel override 2000\n");
				}
				else
					socketSend("DEBUG MAVlink is disabled.  High throttle "
						"ignored.");
			}
			else if (strcmp(buffer, "THROTTLEMINUS") == 0) {
				// hijacked for second uses
				calculate_and_set_flight_path();
				
				/*pulse_width -= 25;
				if (pulse_width >= 2000)
					pulse_width = 2000;
				if (pulse_width <= 1125)
					pulse_width = 1125;
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, pulse_width, pulse_width,
					pulse_width, pulse_width, pulse_width, pulse_width,
					pulse_width, pulse_width);
				if (!mavlink_disabled) {
					mavlink_send(mavlink_message);
					if (verbose || debug) printf("Sent RC channel override with "
						"pulse width %d\n", pulse_width);
				}
				else
					socketSend("DEBUG MAVlink is disabled.  Throttle decrement "
						"ignored.");*/
			}
			else if (strcmp(buffer, "THROTTLEPLUS") == 0) {
				// hijacked for second uses
				start_flight();
				
				/*pulse_width += 25;
				if (pulse_width <= 1125)
					pulse_width = 1125;
				if (pulse_width >= 2000)
					pulse_width = 2000;
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, pulse_width, pulse_width,
					pulse_width, pulse_width, pulse_width, pulse_width,
					pulse_width, pulse_width);
				if (!mavlink_disabled) {
					mavlink_send(mavlink_message);
					if (verbose || debug) printf("Sent RC channel override with "
						"pulse width %d\n", pulse_width);
				}
				else
					socketSend("DEBUG MAVlink is disabled.  Throttle increment "
						"ignored.");*/
			}
			else if (strcmp(buffer, "QUIT") == 0) {
				socket_quit = 1;
				close_controller();
			}
			else if (strcmp(buffer, "FLIGHTPATH") == 0)
				calculate_and_set_flight_path();
			else if (strcmp(buffer, "LAUNCH") == 0)
				start_flight();
			else {
				if (debug || verbose) printf("Socket received an unknown "
					"command: \"%s\"\n", buffer);
			}
			memset(buffer, '\0', 256);
		}
		else if (res == -1)
			; // no data received, may be used in the future
		else if (res == -2) {
			fprintf(stderr, "Socket received failed with error.  The socket "
				"will wait to reconnect or close if requested\n");
			sleep(250); // prevents unnecessary prints if the socket is closing
			socket_closed = 1;
			socketAccept();
		}
	}
	return NULL; // unused but required by P threads
}

void socketAccept() {
	socklen_t size = sizeof(client_addr);
	conn_desc = accept(sock_desc, (struct sockaddr*) &client_addr,
		&size);
	if (conn_desc == -1)
		fprintf(stderr, "Failed to accept connection\n");
	else {
		if (debug || verbose) printf("Socket connection accepted\n");
		socket_closed = 0;
	}
}

int socketSend(char* string) {
	if (write(conn_desc, string, strlen(string)) < strlen(string)) {
		fprintf(stderr, "Failed to send entire string\n");
		return -1;
	}
	return 0;
}

int socketRecv() {
	int res = read(conn_desc, buffer, sizeof(buffer) - 1);
	if (res > 0) // data was written to buffer
		return 0;
	else if (res == 0) // no data received and no error
		return -1;
	else if (res == -1) // socket error
		return -2;
	return 0; // this point shouldn't be reached but we assume no data
}

