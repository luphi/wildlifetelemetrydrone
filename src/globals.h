/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <alsa/asoundlib.h> // audio recording
#include <mavlink.h> // UART serial communication (with pixhawk) library

/*
 * Event and loop variables
 */
extern struct js_event event; // holds information on movements and buttons
extern unsigned char quit; // exit main loop
extern unsigned char verbose; // print useful information
extern unsigned char debug; // print debug information, changes based on revision
extern unsigned char mavlink_disabled; // 1 if pixhawk communication is disabled
extern unsigned char audio_disabled; // 1 if audio recording is disabled
extern unsigned char joystick_disabled; // 1 if PS3 controller input is diabled

/*
 * Logging variables
 */
extern FILE* gps_log; // file handle
extern unsigned char logging; // 0 initially, 1 when recording
extern int logging_start_time; // ticks at which logging began
extern FILE* pcm_log; // file handle for wav/pcm file

/*
 * Audio variables
 */
extern snd_pcm_t* alsa_handle; // connection to alsa's library
extern snd_pcm_hw_params_t* alsa_hw_params; // hardware information from ALSA
extern unsigned char* alsa_buffer; // memory allocated to store PCM
extern int audio_bytes; // number of bytes not including the header

/*
 * UART/MAVlink variables
 */
extern int pixhawk; // handle to serial communication
extern int system_id; // unique (but arbitrary) ID number for this vehicle
extern int component_id; // refers to a component of a system, mostly useless for us
extern int target_system;
extern int target_component;
extern int system_status; // defined by MAVlink; standby, active, emergency, etc.
extern char send_buffer[300]; // 300 is an arbitrary size used in MAVlink examples
extern int message_received; // 0 = nothing received, 1 = something received
extern int pulse_width;
extern mavlink_system_t mavlink_system;
extern mavlink_message_t mavlink_message;

/*
 * Flight variables
 */
extern int current_gps_lat; // GPS lattitude condensed int
extern int current_gps_lon; // GPS longitude condensed int
extern mavlink_message_t waypoints[9];

#endif

