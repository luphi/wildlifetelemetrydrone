/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "controller.h"
#include "mavlink.h"
#include "audio.h"
#include "globals.h"

#include <stdio.h>

/* 
 * Open the handle to the device and determine the number of axes and
 * buttons as well as the driver version and device name.
 * 
 * If successful, the function will return 0.  Otherwise, it has failed.
 */
int init_joystick() {
	int i;
	for(i = 0; i < 19; i++) {
		// initialize the arrays such that they inicate no buttons have been
		// pressed (their use is explained in the input handling function)
		init_presses[i] = 0;
		button_down[i] = 0;
	}

	controller = open(JSDEVICE, O_RDONLY);
	if (controller < 0) {
		// error prints would normally not be verbose mode only but the program
		// loops until the PS3 controller is found so this is verbose only to
		// prevent the program from spamming when run from init.d
		if (verbose) fprintf(stderr, "Failed to open joystick.  This might be "
			"fixed by pressing the PS button.\n");
		return -1;
	}
	
	ioctl(controller, JSIOCGNAME(128), name);
	ioctl(controller, JSIOCGAXES, &axes);
	ioctl(controller, JSIOCGBUTTONS, &buttons);
	ioctl(controller, JSIOCGVERSION, &version);
	
	if (debug || verbose) printf("Using joystick \"%s\" with %d axes and %d "
		"buttons.  Driver version %d.%d.%d.\n", name, axes, buttons,
		version >> 16, (version >> 8) & 0xff, version & 0xff);
	
	return 0;
}

/*
 * Handle joystick events and respond to them, specifically for the PS3
 * controller.  Although it may be adapted for other devices, all events assume
 * values based on a PS3 controller.
 */
int controller_receive() {
	if (read(controller, &event, sizeof(event)) != sizeof(event)) {
		fprintf(stderr, "Error occurred while reading joystick events\n");
		return -1;
	}
	
	// Event properties to care about:
	// event.time = timestamp in milliseconds
	// event.value = value (e.g. how far a joystick is moved)
	// event.type = JS_EVENT_BUTTON, JS_EVENT_AXIS, or JS_EVENT_INIT which
	//      are a button event, joystick event, and no event, respectivelly
	// event.number = the axis/button number, defined in a header
	int type = event.type & ~JS_EVENT_INIT;
	if (type) { // if there was an event
		if (type == JS_EVENT_BUTTON && init_presses[event.number] == 0) {
			// the controller sends one of each event during its
			// initialization so we set each index to 1 to know this phase
			// is done
			init_presses[event.number] = 1;
			// if this conditional isn't reached, the subroutine continues
			// to the switch statement below, ignoring this
		}
		else if (type == JS_EVENT_BUTTON && button_down[event.number]) {
			button_down[event.number] = 0;
		}
		else if (type == JS_EVENT_BUTTON) { // a button was pressed
			button_down[event.number] = 1;
			switch (event.number) {
			case SELECT_BUTTON:
			
				break;
			case L3_BUTTON:
			
				break;
			case R3_BUTTON:
			
				break;
			case START_BUTTON:
				
				break;
			case DPAD_UP:
				pulse_width += 25;
				if (pulse_width <= 1125)
					pulse_width = 1125;
				if (pulse_width >= 2000)
					pulse_width = 2000;
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, pulse_width, pulse_width,
					pulse_width, pulse_width, pulse_width, pulse_width,
					pulse_width, pulse_width);
				mavlink_send(mavlink_message);
				if (verbose || debug) printf("Sent RC channel override with "
					"pulse width %d\n", pulse_width);
				break;
			case DPAD_RIGHT:
			
				break;
			case DPAD_DOWN:
				pulse_width -= 25;
				if (pulse_width >= 2000)
					pulse_width = 2000;
				if (pulse_width <= 1125)
					pulse_width = 1125;
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, pulse_width, pulse_width,
					pulse_width, pulse_width, pulse_width, pulse_width,
					pulse_width, pulse_width);
				mavlink_send(mavlink_message);
				if (verbose || debug) printf("Sent RC channel override with "
					"pulse width %d\n", pulse_width);
				break;
			case DPAD_LEFT:
			
				break;
			case L2_BUTTON:
			
				break;
			case R2_BUTTON:
				
				break;
			case L1_BUTTON:
				logging = 0; // must be set before audio stops
				if (!mavlink_disabled) {
					mavlink_msg_request_data_stream_pack(system_id,
						component_id, &mavlink_message, 1, 0,
						MAV_DATA_STREAM_POSITION, 10, 0); // 0 to stop sending, 1 to start sending
					mavlink_send(mavlink_message);
				}
				if (!audio_disabled)
					stop_audio(); // finish the WAV header
				if (verbose || debug) printf("Ending logging\n");
				break;
			case R1_BUTTON:
				logging = 1; // must be set before audio starts
				// The "interval" parameter (10 below) is actually 1/n rather
				// than n such that higher numbers produce smaller time
				// intervals (a rate).  The target system is 1.
				if (!mavlink_disabled) {
					mavlink_msg_request_data_stream_pack(system_id,
						component_id, &mavlink_message, 1, 0,
						MAV_DATA_STREAM_POSITION, 10, 1); // 1 to start sending, 0 to stop sending
					mavlink_send(mavlink_message);
				}
				if (!audio_disabled)
					start_audio(); // write 99% of the WAV header
				logging_start_time = clock();
				if (verbose || debug) printf("Beginning logging\n");
				break;
			case TRIANGLE_BUTTON:
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, 1125, 1125, 1125, 1125,
					1125, 1125, 1125, 1125);
				mavlink_send(mavlink_message);
				pulse_width = 1125;
				if (verbose || debug) printf("Sent RC channel override 1125\n");
				break;
			case CIRCLE_BUTTON:
				// I spent too many hours trying to get this to work
				// only to discover that the Pixhawk only accepts this command
				// from system 255.  System 255 doesn't even exist.
				// Also, the value is the pulse width in microseconds going from
				// something around 1000 to... I don't know
				mavlink_msg_rc_channels_override_pack(255, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL, 2000, 2000, 2000, 2000,
					2000, 2000, 2000, 2000);
				mavlink_send(mavlink_message);
				pulse_width = 2000;
				if (verbose || debug) printf("Sent RC channel override 2000\n");
				break;
			case X_BUTTON:
				mavlink_msg_command_long_pack(system_id, component_id,
					&mavlink_message, target_system,
					MAV_COMP_ID_SYSTEM_CONTROL,	MAV_CMD_COMPONENT_ARM_DISARM,
					0 /* confirmation */, 1 /* param 1 */, 0, 0, 0, 0, 0, 0);
				mavlink_send(mavlink_message);
				if (verbose || debug) printf("Sent arming command\n");
				break;
			case SQUARE_BUTTON:
				mavlink_msg_command_long_pack(system_id, component_id,
					&mavlink_message, target_system, target_component,
					MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN, 0 /* confirmation */,
					1 /* param 1 */, 1, 0, 0, 0, 0, 0);
				mavlink_send(mavlink_message);
				if (verbose || debug) printf("Sent reboot command\n");
				break;
			case PS_BUTTON:
				return -2; // exit this program
				break;
			default:
				// the PS3 controller reports 19 buttons but I know of only 17
				// so there are reserved values... I just have no idea what they
				// do or if they'll ever be sent
				if (verbose) printf("Unhnadled controller button %d event\n",
					event.number);
				break;
			}
		}
		/*else if ( type == JS_EVENT_AXIS) { // if a joystick was moved
			// note: axes include shape buttons, accelerometer, and D pad
			
		}*/
	}
	return 0;
}

