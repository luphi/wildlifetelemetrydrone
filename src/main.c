/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <limits.h>

#include "mavlink.h"
#include "audio.h"
#include "controller.h"
#include "gpio.h"
#include "globals.h"

unsigned char quit; // exit main loop
unsigned char verbose; // print useful information
unsigned char debug; // print debug information, changes based on revision
unsigned char mavlink_disabled; // 1 if pixhawk communication is disabled
unsigned char audio_disabled; // 1 if audio recording is disabled
unsigned char controller_disabled; // 1 if PS3 controller input is diabled

FILE* gps_log; // file handle
unsigned char logging; // 0 initially, 1 when recording
int logging_start_time; // ticks at which logging began
FILE* pcm_log; // file handle for wav/pcm file

/*
 * Initialize the several components needed, report errors if they exist, and
 * return to main.
 */
int init() {
	if (geteuid() != 0) { // effective user ID (euid) 0 is root, must be root
		printf("This program must be run as root\n");
		return -1;
	}
	
	if (init_gpio() != 0) // if GPIO initialization failed
		return -1;
	
	if (audio_disabled == 0) {
		if (init_audio() != 0) { // if ALSA/audio initialization failed
			fprintf(stderr, "Audio failed to initialize.  It will be "
				"disabled.\n");
			audio_disabled = 1; // it's not working so disable it anyway
		}
	}
	
	// red
	digitalWrite(11, HIGH);

	if (controller_disabled == 0) {
		if (init_controller() != 0) { // if the controller initialization failed
			fprintf(stderr, "Controller input failed to initialize.  It will be"
				" disabled.\n");
			controller_disabled = 1; // it's not working so disable it anyway			
		}
	}
	
	if (mavlink_disabled == 0) {
		if (init_mavlink() != 0) { // if UART/MAVlink initialization failed
			fprintf(stderr, "MAVlink failed to initailize.  It will be "
				"disabled.\n");
			mavlink_disabled = 1; // it's not working so disable it anyway		
		}
	}
		
	// green
	digitalWrite(10, HIGH);
	
	// open gps/compass log file handle
	DIR* dir; // directory
	struct dirent* dent;
	dir = opendir("/home/pi/Code/logs");
	int count = 0; // number of directories in logs directory
	char new_dir[16]; // temporary file name
	if (dir) { // if the logs directory exists
		while ((dent = readdir(dir)) != NULL)
			count += 1;
		sprintf(new_dir, "/home/pi/Code/logs/%d.txt", count - 1);
		gps_log = fopen(new_dir, "w");
		// the log file (most recent) will be named with the highest value
	}
	
	// open audio log file handle
	dir = opendir("/home/pi/Code/wav");
	count = 0;
	if (dir) { // if the wav directory exists
		while ((dent = readdir(dir)) != NULL) // cycle through everything in dir
			count += 1;
		sprintf(new_dir, "/home/pi/Code/wav/%d.wav", count - 1);
		pcm_log = fopen(new_dir, "w");
	}
	
	return 0;
}

/*
 * Exit the main loop and clean up as necessary
 */
void close_and_exit() {
	if (logging && !audio_disabled) // if something happened unexpectedly during logging
		stop_audio(); // finish the WAV file
	digitalWrite(10, LOW); // turn off LED
	digitalWrite(11, LOW); // turn off LED
	// These things *should* be checked before closing/writing but there is no
	// adverse behavior as far as I can tell so all of these will remain until
	// a negative effect is noticed
	if (!mavlink_disabled) {
		fclose(gps_log); // close file handle and finish writing text file
		close(pixhawk); // close handle to Pixhawk
	}
	if (!audio_disabled) {
		fclose(pcm_log); // close file handle and finish writing WAV file
		snd_pcm_close(alsa_handle); // close/unbind the handle
		free(alsa_buffer); // free buffer memory
	}
	if (!controller_disabled)
		close_controller(); // close the socket and thread
	quit = 1; // the main loop will end
}

int main(int argc, char** argv) {
	quit = 0;
	verbose = 0;
	debug = 1;
	mavlink_disabled = 0;
	audio_disabled = 0;
	controller_disabled = 0;
	int i;
	for (i = 1; i < argc; i++) { // argv[0] is the executable name so skip it
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
			printf("usage: %s [-v/--verbose] [-h/--help]\n", argv[0]);
			printf("\t-v/--verbose: Display information related to operation "
				"which may not\n\t\tbe necessary\n");
			printf("\t-d/--debug: Display debug information, for developers\n");
			printf("\t-h/--help: Display this help dialogue\n");
			printf("\t--disable-mavlink: Disable communication with the "
				"Pixhawk\n");
			printf("\t--disable-audio: Disable audio recording\n");
			printf("\t--disable-controller: Disable input from the ground station\n");
			return -1;
		}
		if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0)
			verbose = 1;
		if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--debug") == 0)
			debug = 1;
		if (strcmp(argv[i], "--disable-mavlink") == 0)
			mavlink_disabled = 1;
		if (strcmp(argv[i], "--disable-audio") == 0)
			audio_disabled = 1;
		if (strcmp(argv[i], "--disable-controller") == 0)
			controller_disabled = 1;
	}
	
	if (debug) printf("Initializing systems...\n");
	if (init() != 0)
		close_and_exit();
	if (debug) printf("Initialization complete.  Proceding to main loop"
		".\n");

	int previous_ticks = clock();
	while(!quit) {
		/***********************************************************************
		*
		*  MAVlink block
		*
		***********************************************************************/
		if (mavlink_disabled == 0) {
			if (mavlink_read() != 0)
				close_and_exit();
			mavlink_process_receive();
			if (clock() - previous_ticks >= 1000) {
				// ticks are the number of milliseconds which have passed since
				// the process began so the above 1000 milliseconds means 1
				// second such that this if statement is true about every second
				mavlink_msg_heartbeat_pack(system_id, component_id,
					&mavlink_message, MAV_TYPE_QUADROTOR, MAV_AUTOPILOT_PIXHAWK,
					81, 81, system_status);
				mavlink_send(mavlink_message);
				if (verbose) printf("Sending hearbeat\n");
				previous_ticks = clock();
			}
		}
		
		/***********************************************************************
		*
		*  Audio block
		*
		***********************************************************************/
		/*if (audio_disabled == 0) {	
			if (logging) {
				// Logging GPS and heading data is handled within the MAVlink
				// code so only audio things need to go here
				if (read_audio() != 0)
					close_and_exit();
			}
		}*/
		// Currently, read_audio() is threaded with its own loop so nothing
		// should be done from this main loop
		
		/***********************************************************************
		*
		*  Controller block
		*
		***********************************************************************/
		int con = controller_check();
		if (con == 1 || con == 3) // controller has sent the signal to exit
			close_and_exit();
		// else if () // future use, TBD
	}
	
	return 0;
}
