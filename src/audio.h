/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "globals.h"

#include <pthread.h>
#include <sched.h>

#include <alsa/asoundlib.h>

#define FRAMES_READ 512

struct wave_header {
	char riff[4]; // chunk ID
	int file_size; // total file size - 8
	char wave[4]; // RIFF type
	char fmt[4]; // chunk ID
	int bit_depth; // bit depth
	short format; // compression code
	short channels; // number of channels
	int sample_rate; // sample rate in hertz
	int bytes_per_second; // bytes per second
	short block_align; // block align, relates to bit sample rate and channels
	short bits_per_sample; // bits per smaple
	char data[4]; // chunk ID
	int data_size; // chunk size
};

snd_pcm_t* alsa_handle; // handle to microphone input
unsigned char* alsa_buffer; // holds PCM read from the handle
char device_name[16]; // temporary, used to name the device ("default")
unsigned int channels; // number of channels, 1 or 2
unsigned int rate; // sampling rate, CDs are 44100, 48000 is also reasonable
int stream; // stream type (capture, as opposed to playback)
int acc; // access, interleaved vs non-interleaved, we do interleaved
int format; // PCM format type (e.g. little endian, 16 bit)
int audio_bytes; // number of total bytes written (of the frames, not header)
pthread_t thread; // audio recording is done in a thread

/*
 * Initialize handles and otherwise get the program ready to read from the
 * chipset buffer.
 */
int init_audio();

/*
 * Writes WAV header information such that decoders can play it properly.
 * This function is dependent on a couple variables (channels and rate) and will
 * adapt accordingly.
 *
 * Starts the read thread as well.
 */
void start_audio();

/*
 * Writes the data/file size information into the header.  Must be called
 * before the file handle is closed.
 */
void stop_audio();

/*
 * Reads and writes the audio while still logging.  Will run continuously in its
 * own thread.
 *
 * Reads FRAMES_READ frames of audio.  This value may be increased if necessary
 * but the buffer must be increased with it.
 */
void* read_audio(void* p);
//int read_audio();
