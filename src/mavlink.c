/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
 #include "mavlink.h"
 #include "globals.h"

#include <stdio.h>
#include <termios.h>

/*
 * Initialize UART communication for use with the MAVlink protocol/library.
 * The MAVlink library itself doesn't need to be initialized but all related
 * initialization is handled here.
 *
 * If successful, the function will return 0.  Otherwise, it has failed.
 */
int init_mavlink() {
	system_id = 9; // number of members of this capstone team
	component_id = 9; // arbitrary values, MAVlink doesn't currently use IDs
	target_system = 0;
	target_component = 0;
	pulse_width = 1125;
	system_status = MAV_STATE_STANDBY;

	pixhawk = open(UARTDEVICE, O_RDWR | O_NOCTTY | O_NDELAY);
	if (pixhawk == -1) { // failure
		fprintf(stderr, "Failed to open serial communication with the port/"
			"device.  This most likely indicates loose wires.\n");
		return -1;
	}
	
	fcntl(pixhawk, F_SETFL, O_NONBLOCK); // UART reading will be nonblocking
	
	if (!isatty(pixhawk)) {
		fprintf(stderr, "Descriptor for pixhawk is not recognized as a serial "
			"port\n");
		return -1;
	}
	
	struct termios config;
	if (tcgetattr(pixhawk, &config) < 0) {
		fprintf(stderr, "Unable to read pixhawk terminal configuration\n");
		return -1;
	}
	
	/*
	 * The following is configuration for UART.  Unlike what you'll do in
	 * EE 410, this isn't an overly-complicated process at the register level.
	 * The flags below are all that's necessary.  The flags are all documentated
	 * online.
	 */
	config.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
	config.c_iflag = IGNPAR;
	config.c_oflag = 0;
	config.c_lflag = 0;
	 
	if (tcsetattr(pixhawk, TCSAFLUSH, &config) < 0) {
		fprintf(stderr, "Could not set UART configuration\n");
		return -1;
	}
	
	mavlink_system.sysid = system_id;
	mavlink_system.compid = component_id;
	 
	message_received = 0;
	int i;
	for (i = 0; i < 300; i++)
		send_buffer[i] = 0;
	
	return 0;
}

/*
 * Send a MAVlink message over UART.  This is a blocking function, meaning it
 * halts processing until the sending is complete.  It blocks for a very short
 * time by human standards but there's a chance the blocking may prevent, for
 * example, input handling from the controller.
 *
 * In the future, this should probably be threaded.
 */
void mavlink_send(mavlink_message_t message) {
	int len = mavlink_msg_to_send_buffer((uint8_t*) send_buffer, &message);
	write(pixhawk, send_buffer, len);
	//tcflush(pixhawk, TCIFLUSH); // see below
	tcdrain(pixhawk);
	// The terminal functions tcflush() and tcdrain() both relate to write(),
	// specifically when writing to a termainl (the UART port in this case).
	// Whereas tcdrain() will wait until all data is written, and I realize this
	// makes sense at first consideration, tcflush() discards unwritten data
	// following the return of write().
	// The official MAVlink example uses tcdrain() but, upon reading a
	// discussion among WINE developers of all things, I decided tcflush() is
	// more appropriate.  Then again, neither is exactly necessary.
}

/*
 * This is a UART-related function which does little more than check for UART
 * data.  The processing of this data is handled by another function.
 *
 * In the future, this should probably be threaded with O_NONBLOCK disabled.
 */
int mavlink_read() {
	char buffer[1024]; // some MAVlink messages are big
	mavlink_status_t status;
	mavlink_status_t lastStatus;
	lastStatus.packet_rx_drop_count = 0;
	int num = read(pixhawk, &buffer, sizeof(buffer));
	if (num > 0) { // read() returns number of bytes read
		int i; //, j;
		for (i = 0; i < num; i++) {
			//for (j = 0; j < 128; j++) {
				if (mavlink_parse_char(0 /* chan */, (uint8_t)(buffer[i]),
						&mavlink_message, &status)) {
					message_received = 1;
					break;
				}
			//}
		}
		if (verbose && (lastStatus.packet_rx_drop_count !=
				status.packet_rx_drop_count)) {
			fprintf(stderr, "Dropped %d UART packet",
				status.packet_rx_drop_count);
			if (status.packet_rx_drop_count == 1)
				printf("\n");
			else
				printf("s\n"); // plurality
		}
		lastStatus = status;
		//if (verbose && message_received) printf("\tReceived MAVlink message "
		//	"on index %d\n", i);
	}
	else if (num < 0 && errno != EAGAIN) {
		// in the case of EAGAIN, we can ignore this as a byproduct of using a
		// nonblocking handle as this error essentially means there was nothing
		// to read so the operation stopped but please note that, unlike EAGAIN
		// with other functions, this does not mean the buffer is full
		fprintf(stderr, "Unable to read from UART to pixhawk: %s\n",
			strerror(errno));
		return -1;
	} // a return value of 0 indicates nothing
	return 0;
}

/*
 * If a message has been received, as determined by mavlink_read(), process the
 * the message accordingly.  This is not a full list of MAVlink messages but
 * is/will be all the ones pertaining to this drone.
 *
 * The message itself is stored in the global variable mavlink_message.
 */
void mavlink_process_receive() {
	if (message_received) {
		// get the targets if they're different
		if (mavlink_message.sysid != target_system || mavlink_message.compid !=
				target_component) {
			target_system = mavlink_message.sysid;
			target_component = mavlink_message.compid;
		}
		// possibly required variables which cannot be declared in a switch:
		mavlink_ping_t ping;
		mavlink_system_time_t time;
		mavlink_param_set_t set;
		mavlink_heartbeat_t heartbeat;
		mavlink_nav_controller_output_t nav_controller_output;
		mavlink_rc_channels_raw_t rc_channels;
		mavlink_raw_imu_t raw_imu;
		mavlink_sys_status_t sys_status;
		mavlink_statustext_t statustext;
		mavlink_global_position_int_t gps_int;
		mavlink_command_ack_t command_ack;
		mavlink_vfr_hud_t vfr_hud;
		mavlink_mission_current_t mission_current;
		mavlink_scaled_pressure_t scaled_pressure;
		mavlink_mission_request_t mission_request;
		mavlink_mission_ack_t mission_ack;
		mavlink_mission_item_reached_t mission_reached;
		//if (verbose) printf("Received MAVlink message with ID %d "
		//	"(sys.%d|comp.%d)\n", mavlink_message.msgid, mavlink_message.sysid,
		//	mavlink_message.compid);
		switch (mavlink_message.msgid) {
			case MAVLINK_MSG_ID_HEARTBEAT:
				mavlink_msg_heartbeat_decode(&mavlink_message, &heartbeat);
				system_status = heartbeat.system_status;
				char status[32];
				switch (heartbeat.system_status) {
				case MAV_STATE_UNINIT:
					sprintf(status, "MAV_STATE_UNINIT");
					break;
				case MAV_STATE_BOOT:
					sprintf(status, "MAV_STATE_BOOT");
					break;
				case MAV_STATE_CALIBRATING:
					sprintf(status, "MAV_STATE_CALIBRATING");
					break;
				case MAV_STATE_STANDBY:
					sprintf(status, "MAV_STATE_STANDBY");
					break;
				case MAV_STATE_ACTIVE:
					sprintf(status, "MAV_STATE_ACTIVE");
					break;
				case MAV_STATE_CRITICAL:
					sprintf(status, "MAV_STATE_CRITICAL");
					break;
				case MAV_STATE_EMERGENCY:
					sprintf(status, "MAV_STATE_EMERGENCY");
					break;
				case MAV_STATE_POWEROFF:
					sprintf(status, "MAV_STATE_POWEROFF");
					break;
				default:
					break; // there should be no other used states
				}
				if (verbose) printf("MAVLINK_MSG_ID_HEARTBEAT: received a "
					"heartbeat from MAVlink version %hhu, system status %s, "
					"and base mode %hhu\n", heartbeat.mavlink_version, status,
					heartbeat.base_mode);
				break;
			case MAVLINK_MSG_ID_COMMAND_ACK:
				mavlink_msg_command_ack_decode(&mavlink_message, &command_ack);
				char result[32];
				switch (command_ack.result) {
				case MAV_RESULT_ACCEPTED:
					sprintf(result, "MAV_RESULT_ACCEPTED");
					break;
				case MAV_RESULT_TEMPORARILY_REJECTED:
					sprintf(result, "MAV_RESULT_TEMPORARILY_REJECTED");
					break;
				case MAV_RESULT_DENIED:
					sprintf(result, "MAV_RESULT_DENIED");
					break;
				case MAV_RESULT_UNSUPPORTED:
					sprintf(result, "MAV_RESULT_UNSUPPORTED");
					break;
				case MAV_RESULT_FAILED:
					sprintf(result, "MAV_RESULT_FAILED");
					break;
				default:
					break; // there should be no other command acknowledgements
				}
				printf("MAVLINK_MSG_ID_COMMAND_ACK: %hu %s\n",
					command_ack.command, result);
				break;
			case MAVLINK_MSG_ID_VFR_HUD:
				mavlink_msg_vfr_hud_decode(&mavlink_message, &vfr_hud);
				if (verbose) printf("MAVLINK_MSG_ID_VFR_HUD:\n"
					"\tairspeed (meters/second):    %f\n"
					"\tgroundpseed (meters/second): %f\n"
					"\taltitude (meters):           %f\n"
					"\tclimb (meters/second):       %f\n"
					"\theading (degrees):           %hd\n"
					"\tthrottle (percentage):       %hu\n",
					vfr_hud.airspeed, vfr_hud.groundspeed, vfr_hud.alt,
					vfr_hud.climb, vfr_hud.heading, vfr_hud.throttle);
				break;
			case MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT:
				mavlink_msg_nav_controller_output_decode(&mavlink_message,
					&nav_controller_output);
				if (verbose) printf("MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT:\n"
					"\tdesired roll (degrees):         %f\n"
					"\tdesired pitch (degrees):        %f\n"
					"\taltitude error (meters):        %f\n"
					"\tairspeed error (meters/second): %f\n"
					"\tcrosstrack error (meters):      %f\n"
					"\tdesired heading (degrees):      %hd\n"
					"\tcurrent bearing (degrees):      %hd\n"
					"\tdistance to go (meters):        %hu\n",
					nav_controller_output.nav_roll,
					nav_controller_output.nav_pitch,
					nav_controller_output.alt_error,
					nav_controller_output.aspd_error,
					nav_controller_output.xtrack_error,
					nav_controller_output.nav_bearing,
					nav_controller_output.target_bearing,
					nav_controller_output.wp_dist);
				break;
			case MAVLINK_MSG_ID_STATUSTEXT:
				mavlink_msg_statustext_decode(&mavlink_message, &statustext);
				if (verbose || debug) printf("MAVLINK_MSG_ID_STATUSTEXT: "
					"(severity %d) \"%s\"\n", statustext.severity,
					statustext.text);
				break;
			case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
				mavlink_msg_global_position_int_decode(&mavlink_message,
					&gps_int);
				// Global variables used for flight path calculation
				current_gps_lat = gps_int.lat;
				current_gps_lon = gps_int.lon;
				if (verbose) printf("MAVLINK_MSG_ID_GLOBAL_POSITION_INT:\n"
					"\tGPS location:           (%d, %d)\n"
					"\taltitude:               %d\n"
					"\trelative atitude:       %d\n"
					"\theading (degrees):      %hu\n",
					 gps_int.lat, gps_int.lon, gps_int.alt,
					 gps_int.relative_alt,
					(gps_int.hdg == 65535 ? 65535 : gps_int.hdg / 100));
				//if (debug) printf("MAVLINK_MSG_ID_GLOBAL_POSITION_INT:\n"
					//"\t(%d, %d)\n"
					//"\t%hu degrees\n", gps_int.lat, gps_int.lon,
					//(gps_int.hdg == 65535 ? 65535 : gps_int.hdg / 100));
				if (logging) fprintf(gps_log, "%d %d %d %hu\n",
					(clock() - logging_start_time) / 10000, gps_int.lat,
					gps_int.lon, (gps_int.hdg == 65535 ? 65535 :
					gps_int.hdg / 100));
				break;
			case MAVLINK_MSG_ID_RC_CHANNELS_RAW:
				mavlink_msg_rc_channels_raw_decode(&mavlink_message,
					&rc_channels);
				if (verbose) printf("MAVLINK_MSG_ID_RC_CHANNELS:\n"
					"\tchannel 1: %u\n"
					"\tchannel 2: %u\n"
					"\tchannel 3: %u\n"
					"\tchannel 4: %u\n"
					"\tchannel 5: %u\n"
					"\tchannel 6: %u\n"
					"\tchannel 7: %u\n"
					"\tchannel 8: %u\n"
					"\tport: %hhu, RSSI: %hhu%\n", rc_channels.chan1_raw,
					rc_channels.chan2_raw, rc_channels.chan3_raw,
					rc_channels.chan4_raw, rc_channels.chan5_raw,
					rc_channels.chan6_raw, rc_channels.chan7_raw,
					rc_channels.chan8_raw, rc_channels.port,
					rc_channels.rssi);
				break;
			case MAVLINK_MSG_ID_RAW_IMU:
				mavlink_msg_raw_imu_decode(&mavlink_message, &raw_imu);
				if (verbose) printf("MAVLINK_MSG_ID_RAW_IMU:\n"
					"\tX acceleration:   %hd\n"
					"\tY acceleration:   %hd\n"
					"\tZ acceleration:   %hd\n"
					"\tX angular speed:  %hd\n"
					"\tY angular speed:  %hd\n"
					"\tZ angular speed:  %hd\n"
					"\tX magnetic field: %hd\n"
					"\tY magnetic field: %hd\n"
					"\tZ magnetic field: %hd\n", raw_imu.xacc, raw_imu.yacc,
					raw_imu.zacc, raw_imu.xgyro, raw_imu.ygyro, raw_imu.zgyro,
					raw_imu.xmag, raw_imu.ymag, raw_imu.zmag);
				break;
			case MAVLINK_MSG_ID_PING:
				// this type is defined as:
				// typedef struct __mavlink_ping_t
				// {
				//     uint64_t time_usec;
				//     uint32_t seq; // ping sequence
				//     uint8_t target_system;
				//     uint8_t target_component;
				// } mavlink_ping_t
				mavlink_msg_ping_decode(&mavlink_message, &ping);
				if (verbose) printf("MAVLINK_MSG_ID_PING: received a ping with "
					"sequence %u\n", ping.seq);
				if (!ping.target_system && !ping.target_component) {
					// if the system and component targets are set, the device
					// is pinging every connected system and I don't really care
					// about that situation
					mavlink_msg_ping_pack(system_id, component_id,
						&mavlink_message, ping.time_usec, ping.seq,
						mavlink_message.sysid, mavlink_message.compid);
					mavlink_send(mavlink_message);
				}
				break;
			case MAVLINK_MSG_ID_SYS_STATUS:
				mavlink_msg_sys_status_decode(&mavlink_message, &sys_status);
				if (verbose) printf("MAVLINK_MSG_ID_SYS_STATUS:\n"
					"\tPixhawk load:      %hu%\n"
					"\tBattery voltage:   %f V\n"
					"\tBattery current:   %f A\n"
					"\tBattery remaining: %hhd%\n",
					sys_status.load,
					((float) sys_status.voltage_battery) / 1000.0,
					((float) sys_status.current_battery) / 1000.0,
					sys_status.battery_remaining);
					break;
			case MAVLINK_MSG_ID_SYSTEM_TIME:
				// system time on the Pixhawk
				// this may be entirely useless
				// this type is defined as:
				// typedef struct __mavlink_system_time_t
				// {
				//     uint64_t time_usec;
				// } mavlink_system_time_t
				// so it's just UNIX time
				mavlink_msg_system_time_decode(&mavlink_message, &time);
				if (verbose) printf("MAVLINK_MSG_ID_SYSTEM_TIME: received "
					"system time information: %u (system uptime)\n",
					time.time_boot_ms);
				break;
			case MAVLINK_MSG_ID_PARAM_SET:
				// typdef struct __mavlink_param_set_t
				// {
				//     uint8_t target_system;
				//     uint8_t target_component;
				//     int8_t param_id[15]; // onboard parameter ID
				//     float param_value; // onboard parameter value
				// } mavlink_param_set_t
				mavlink_msg_param_set_decode(&mavlink_message, &set);
				char ids[128] = "[ ";
				char temp[8] = "255 ";
				int i;
				for (i = 0; i < 16; i++) {
					sprintf(temp, "%hhd ", set.param_id[i]);
					strcat(ids, temp);
				}
				strcat(ids, "]");
				if (verbose) printf("MAVLINK_MSG_ID_PARAM_SET: received "
					"parameter set of value %f and IDs %s\n", set.param_value,
					ids);
				break;
			case MAVLINK_MSG_ID_MISSION_CURRENT:
				mavlink_msg_mission_current_decode(&mavlink_message,
					&mission_current);
				if (verbose) printf("MAVLINK_MSG_ID_MISSION_CURRENT:\n"
					"%hu\n", mission_current.seq);
				break;
			case MAVLINK_MSG_ID_ATTITUDE:
				
				break;
			case MAVLINK_MSG_ID_GPS_RAW_INT:
			
				break;
			case MAVLINK_MSG_ID_SCALED_IMU:
				
				break;
			case MAVLINK_MSG_ID_SCALED_PRESSURE:
				mavlink_msg_scaled_pressure_decode(&mavlink_message,
					&scaled_pressure);
				if (verbose) printf("MAVLINK_MSG_ID_SCALED_PRESSURE:\n"
					"\tAbsolute pressure:     %f\n"
					"\tDifferential pressure: %f\n"
					"\tTemperature (C):       %hd\n", scaled_pressure.press_abs,
					scaled_pressure.press_diff, scaled_pressure.temperature);
				break;
			case MAVLINK_MSG_ID_MISSION_REQUEST:
				mavlink_msg_mission_request_decode(&mavlink_message,
					&mission_request);
				if (debug || verbose) printf("MAVLINK_MSG_ID_MISSION_REQUEST:\n"
					"\tThe Pixhawk requested mission sequence %d\n",
					mission_request.seq);
				mavlink_send(waypoints[mission_request.seq]);
				break;
			case MAVLINK_MSG_ID_MISSION_ACK:
				mavlink_msg_mission_ack_decode(&mavlink_message, &mission_ack);
				char ack_result[32];
				switch (mission_ack.type) {
				case MAV_MISSION_ACCEPTED:
					sprintf(ack_result, "MAV_MISSION_ACCEPTED");
					break;
				case MAV_MISSION_ERROR:
					sprintf(ack_result, "MAV_MISSION_ERROR (generic error)");
					break;
				case MAV_MISSION_UNSUPPORTED_FRAME:
					sprintf(ack_result, "MAV_MISSION_UNSUPPORTED_FRAME");
					break;
				case MAV_MISSION_UNSUPPORTED:
					sprintf(ack_result, "MAV_MISSION_UNSUPPORTED (unsupported "
						"command)");
					break;
				case MAV_MISSION_NO_SPACE:
					sprintf(ack_result, "MAV_MISSION_NO_SPACE (mission item "
						"exceeds storage space");
					break;
				case MAV_MISSION_INVALID:
					sprintf(ack_result, "MAV_MISSION_INVALID (one of the "
						"parameters has an invalid value)");
					break;
				case MAV_MISSION_INVALID_PARAM1:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM1");
					break;
				case MAV_MISSION_INVALID_PARAM2:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM2");
					break;
				case MAV_MISSION_INVALID_PARAM3:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM3");
					break;
				case MAV_MISSION_INVALID_PARAM4:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM4");
					break;
				case MAV_MISSION_INVALID_PARAM5_X:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM5_X");
					break;
				case MAV_MISSION_INVALID_PARAM6_Y:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM6_Y");
					break;
				case MAV_MISSION_INVALID_PARAM7:
					sprintf(ack_result, "MAV_MISSION_INVALID_PARAM7");
					break;
				case MAV_MISSION_INVALID_SEQUENCE:
					sprintf(ack_result, "MAV_MISSION_INVALID_SEQUENCE");
					break;
				case MAV_MISSION_DENIED:
					sprintf(ack_result, "MAV_MISSION_DENIED (not accepting any "
						"mission commands from this communication partner)");
					break;
				default:// only MAV_MISSION_RESULT_ENUM_END is left out but...
					sprintf(ack_result, "an unknown result value (%d)",
						mission_ack.type);
					break;
				}
				if (debug || verbose) printf("MAVLINK_MSG_ID_MISSION_ACK:\n"
					"\tThe Pixhawk reported %s\n", ack_result);
				break;
			case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
				mavlink_msg_mission_item_reached_decode(&mavlink_message,
					&mission_reached);
				if (debug || verbose) printf("MAVLINK_MSG_ID_MISSION_ITEM_REACH"
					"ED:\n"
					"\tThe Pixhawk has reached mission item %d\n",
					mission_reached.seq);
				break;
			default:
				printf("Received an unhandled MAVlink command: %d\n",
					mavlink_message.msgid);
				break;
		}
		message_received = 0;
	}
}

