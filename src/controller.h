/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <pthread.h>
#include <netinet/in.h>

int sock_desc, conn_desc;
struct sockaddr_in serv_addr, client_addr;
char buffer[256];

pthread_t thread;
int socket_quit;
int socket_closed;
unsigned int last_ping;

int pulse_width;

/* 
 * Open a socket connection with the ground station (a tablet app) and
 * initialize the thread on which controller-related procedures run.
 * 
 * If successful, the function will return 0.  Otherwise, it has failed.
 */
int init_controller();

/*
 * Close the socket and exit the thread safely.
 */
void close_controller();

/*
 * Returns an integer indicating the status of the communication with
 * the ground station.
 * 
 * 0: Socket connected and no action requested
 * 1: Socket connected quit was requested
 * 2: Socket not connected but no action requested, will wait for connection
 * 3: Socket not connected and quit requested
 */
int controller_check();

/*
 * Threaded function used internally for communication with the ground
 * station.
 */
void* socketRun(void* p);

/*
 * Used internally in the event that the socket should listen and accept
 * a new connection.  Blocking.
 */
void socketAccept();

/*
 * Used internally to send a string to the ground station.
 * 
 * Returns 0 if successful, otherwise returns -1.
 */
int socketSend(char* string);

/*
 * Used internally to receive a string from the ground station.  The
 * data is written into the buffer variable.
 * 
 * Returns 0 if successful, -1 if there is no data to read, or -2 in the
 * case of an error.
 */
int socketRecv();

#endif
