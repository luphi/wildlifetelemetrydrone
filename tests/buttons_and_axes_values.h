#define SELECT_BUTTON         0
#define LEFT_JOYSTICK_BUTTON  1
#define RIGHT_JOYSTICK_BUTTON 2
#define START_BUTTON          3
#define DPAD_UP               4
#define DPAD_RIGHT            5
#define DPAD_DOWN             6
#define DPAD_LEFT             7
#define L2_BUTTON             8
#define R2_BUTTON             9
#define L1_BUTTON             10
#define R1_BUTTON             11
#define TRIANGLE_BUTTON       12
#define CIRCLE_BUTTON         13
#define X_BUTTON              14
#define SQUARE_BUTTON         15
#define PS_BUTTON             16

