/* 
 * Copyright (c) 2014, Lucas Philipsen, Trevor Petersen, Willem Arjana,
 *     Leonard Peshlakasi, Reejay Martinez, Dylan Steyer
 * All rights reserved.
 * 
 * Redistribution and use in source and binarys forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Lucas
 * Philipsen, Trevor Petersen, Willem Arjana, Leonard Peshlakasi, Reejay
 * Martinez, NOR Dylan Steyer BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLAYER, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <fcntl.h>

#include <linux/joystick.h>
#include <sys/ioctl.h>

#include "buttons_and_axes_values.h"

#define DEVICE "/dev/input/js0" // "/dev" = devices, "js0" = joystick 0

/*
 * Event and loop variables
*/
struct js_event event; // holds information on movements and buttons

/*
 * Joystick variables
 */
int controller; // the handle to the device
unsigned char axes; // number of axes/joysticks on the device
unsigned char buttons; // number of buttons on the device
int version; // driver version
char name[128]; // name of the device

/* 
 * Open the handle to the device and determine the number of axes and
 * buttons as well as the driver version and device name.  Upon success,
 * 0 will be returned.  If there is an error, -1 will be returned along
 * with an error message.
 * 
 * The variables holding this information are defined above.
 */
int init_joystick() {
	controller = open(DEVICE, O_RDONLY);
	if (controller < 0) {
		fprintf(stderr, "Failed to open joystick.  This might be fixed "
			"by pressing the PS button.\n");
		return -1;
	}
	
	ioctl(controller, JSIOCGNAME(128), name);
	ioctl(controller, JSIOCGAXES, &axes);
	ioctl(controller, JSIOCGBUTTONS, &buttons);
	ioctl(controller, JSIOCGVERSION, &version);
	
	printf("Using joystick \"%s\" with %d axes and %d buttons.  Driver "
		"version %d.%d.%d.\n", name, axes, buttons, version >> 16,
		(version >> 8) & 0xff, version & 0xff);
	
	return 0;
}

int main(int argc, char** argv) {
	if (init_joystick() != 0) // if initialization fialed
		return -1;
	
	for (;;) {
		if (read(controller, &event, sizeof(event)) != sizeof(event)) {
			fprintf(stderr, "Error occurred while reading events\n");
			return -1;
		}
		
		// Event properties to care about:
		// event.time = timestamp in milliseconds
		// event.value = value (e.g. how far a joystick is moved)
		// event.type = JS_EVENT_BUTTON, JS_EVENT_AXIS, or JS_EVENT_INIT which
		//      are a button event, joystick event, and no event, respectivelly
		// event.number = the axis/button number, defined in a header
		int type = event.type & ~JS_EVENT_INIT;
		if (type) { // if there was an event
			if (type == JS_EVENT_BUTTON) { // a button was pressed
				printf("event.number = %d\n", event.number);
			}
			else if ( type == JS_EVENT_AXIS) { // if a joystick was moved
				// note: axes include shape buttons, accelerometer, and D pad
			
			}
		}
	}
	
	return 0;
}
