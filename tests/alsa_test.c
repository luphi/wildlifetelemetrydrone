/* 
 * Copyright (c) 2015, Luke Philipsen
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification are permitted provided that the following conditions
 * are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of Northern Arizona University nor the names
 *      of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written
 *      permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL Luke Philipsen BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLAYER, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <time.h>

#include <alsa/asoundlib.h>

struct wave_header {
	char riff[4]; // chunk ID
	int file_size; // total file size - 8
	char wave[4]; // RIFF type
	char fmt[4]; // chunk ID
	int bit_depth; // bit depth
	short format; // compression code
	short channels; // number of channels
	int sample_rate; // sample rate in hertz
	int bytes_per_second; // bytes per second
	short block_align; // block align, relates to bit sample rate and channels
	short bits_per_sample; // bits per smaple
	char data[4]; // chunk ID
	int data_size; // chunk size
};

snd_pcm_t* read_handle;
snd_pcm_t* write_handle;
unsigned char* buffer;
char device_name[16];
unsigned int channels;
unsigned int rate;
int stream;
int acc;
int format;
FILE* pcm_log;
int audio_bytes;

int init_audio() {
	snd_pcm_hw_params_t* hw_params;
	int res;
	
	res = snd_pcm_open(&read_handle, device_name, stream, 0);
	if (res < 0) {
		fprintf(stderr, "Failed to open handle to \"default\" (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	//res = snd_pcm_nonblock(read_handle, 1);
	//if (res < 0) {
	//	fprintf(stderr, "Couldn't set audio handel to nonblock (%s)\n",
	//		snd_strerror(res));
	//	return -1;
	//}
	
	res = snd_pcm_hw_params_malloc(&hw_params);
	if (res < 0) {
		fprintf(stderr, "Failed to allocate hardware parameters structure "
			"(%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_any(read_handle, hw_params);
	if (res < 0) {
		fprintf(stderr, "Couldn't initialize hardware parameters structure "
			"(%s)\n", snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_access(read_handle, hw_params, acc);
	if (res < 0) {
		fprintf(stderr, "Couldn't set audio access type (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_format(read_handle, hw_params, format);
	if (res < 0) {
		fprintf(stderr, "Couldn't set PCM format type (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_channels(read_handle, hw_params, channels);
	if (res < 0) {
		fprintf(stderr, "Couldn't set number of channels to %d (%s)\n",
			channels, snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params_set_rate_near(read_handle, hw_params, &rate, 0);
	if (res < 0) {
		fprintf(stderr, "Couldn't set rate near %d (%s)\n", rate,
			snd_strerror(res));
		return -1;
	}
	
	res = snd_pcm_hw_params(read_handle, hw_params);
	if (res < 0) {
		fprintf(stderr, "Failed to set audio hardware parameters (%s)\n",
			snd_strerror(res));
		return -1;
	}
	
	snd_pcm_hw_params_free(hw_params);
	
	//res = snd_pcm_prepare(read_handle);
	//if (res < 0) {
		//fprintf(stderr, "Couldn't prepare audio interface for use (%s)\n",
			//snd_strerror(res));
		//return -1;
	//}
	
	buffer = (unsigned char*) malloc(2 * channels * 128);

	return 0;
}

void write_header() {
	struct wave_header header;

	header.riff[0] = 'R';
	header.riff[1] = 'I';
	header.riff[2] = 'F';
	header.riff[3] = 'F';
	header.file_size = 0; // will be rewritten later
	header.wave[0] = 'W';
	header.wave[1] = 'A';
	header.wave[2] = 'V';
	header.wave[3] = 'E';
	header.fmt[0] = 'f';
	header.fmt[1] = 'm';
	header.fmt[2] = 't';
	header.fmt[3] = ' ';
	header.bit_depth = 16; // 16-bit
	header.format = 1; // 1 is for integer PCM
	header.channels = channels; // number of channels
	header.sample_rate = rate; // sample rate in hertz
	header.bytes_per_second = rate * 16 * channels / 8; // rate * bits per sample * channels / 8
	header.block_align = 2 * channels; // bits per sample * channels / 8
	header.bits_per_sample = 16;
	header.data[0] = 'd';
	header.data[1] = 'a';
	header.data[2] = 't';
	header.data[3] = 'a';
	header.data_size = 0; // will be rewritten later
	
	// A wave header is 44 bytes and we're writing 1
	fwrite(&header, 44, 1, pcm_log);
}

void finish_header() {
	// The first chunk data size is given as file size - 8 where file size is
	// data + header.  This information is 4 bytes in size located at 0x0004.
	unsigned int file_size = ((unsigned int) audio_bytes +
		sizeof(struct wave_header) - 8);
	fseek(pcm_log, 4, SEEK_SET); // begin writing at byte 5
	fwrite(&file_size, 4, 1, pcm_log); // write 4 bytes
	// The next data size is the size of the data itself excluding header bytes
	// which is just audio_bytes.  This is located at byte 41 (0x0028)
	unsigned int data_size = ((unsigned int) audio_bytes);
	fseek(pcm_log, 40, SEEK_SET); // begin writing at byte 41
	fwrite(&data_size, 4, 1, pcm_log); // write 4 bytes
}

void error_handle() {
	snd_pcm_status_t* status;
	int res;
	
	snd_pcm_status_malloc(&status);
	snd_pcm_status(write_handle, status);
	if (res < 0) {
		fprintf(stderr, "Write status error (%s)\n", snd_strerror(res));
		return;
	}
	
	if (snd_pcm_status_get_state(status) == SND_PCM_STATE_XRUN) {
		
	}
	else if (snd_pcm_status_get_state(status) == SND_PCM_STATE_DRAINING) {
		// only for capture errors?
		printf("DRAINING STATE ERROR\n");
	}
}

int main() {
	sprintf(device_name, "default");
	channels = 1;
	rate = 44100;
	stream = SND_PCM_STREAM_CAPTURE;
	acc = SND_PCM_ACCESS_RW_INTERLEAVED;
	format = SND_PCM_FORMAT_S16_LE;
	audio_bytes = 0;
	
	pcm_log = fopen("alsa_test.wav", "w");
	write_header();

	if (init_audio() != 0)
		fprintf(stderr, "INIT ERROR\n");

	int start = clock();
	int i, res;
	while (clock() - start < 1000000) {
		res = snd_pcm_readi(read_handle, buffer, 128);
		if (res > 0) {
			int bytes = res * channels * 2; // because 16 bits / 8 = 2 bytes
			audio_bytes += bytes;
			fwrite(buffer, bytes, 1, pcm_log);
		}
	}

	finish_header();

	snd_pcm_close(read_handle);
	fclose(pcm_log);
	free(buffer);

	return 0;
}

