

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

int sock_desc, conn_desc;
struct sockaddr_in serv_addr, client_addr;
char buffer[128];

int socketSend(char* string) {
	if (write(conn_desc, string, strlen(string)) < strlen(string))
		fprintf(stderr, "Failed to send entire string\n");
	
	printf("socketSend(): \"%s\"\n", string);
	return 0;
}

int main() {
	sock_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_desc < 0)
		fprintf(stderr, "Failed to create socket\n");
	
	bzero((char*) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(6668);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	if (bind(sock_desc, (struct sockaddr*) &serv_addr,
			sizeof(serv_addr)) < 0)
		fprintf(stderr, "Failed to bind socket\n");
	
	listen(sock_desc, 100);
	printf("Listening...\n");
	int size = sizeof(client_addr);
	conn_desc = accept(sock_desc, (struct sockaddr*) &client_addr,
		&size);
	if (conn_desc == -1)
		fprintf(stderr, "Failed to accpet connection\n");
	else
		printf("Socket connection established\n");
	
	int quit = 0;
	while (!quit) {
		if (read(conn_desc, buffer, sizeof(buffer) - 1) > 0) {
			if (strstr(buffer, "PING") != NULL) {
				char temp[16];
				char temp2[32];
				strcpy(temp, buffer + 5);
				sprintf(temp2, "PONG %s", temp);
				socketSend(temp2);
			}
			else if (strcmp(buffer, "ARM") == 0) {
				printf("\tArm command received\n");
			}
			else if (strcmp(buffer, "DISARM") == 0) {
				printf("\tDisarm command received\n");
			}
			else if (strcmp(buffer, "STARTRECORD") == 0) {
				printf("\tStart record command received\n");
			}
			else if (strcmp(buffer, "STOPRECORD") == 0) {
				printf("\tStop record command received\n");
			}
			else if (strcmp(buffer, "QUIT") == 0) {
				quit = 1;
			}
			else {
				printf("\tReceived unknown command: %s\n", buffer);
			}
			memset(buffer, '\0', 128);
		}
	}
	
	close(conn_desc);
	close(sock_desc);
	
	return 0;
}
