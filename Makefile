### to use this, just type "make"
### to rebuild everything, run "make clean" first

CC=gcc -g

CFLAGS= -fPIC -I. -I/usr/local/include \
		-I/home/pi/mavlink/include -I/home/pi/mavlink/include/common \
		-I/home/pi/mavlink/include/pixhawk -Wall -O2
	
LDFLAGS=-lwiringPiDev \
		-lwiringPi \
		-lasound \
		-pthread
		
BINARY=wtd
BIN=./bin
SRC=./src

### If you want to use other source files, add an object file with an
### appropriate name to this list.  For example:
### OBJECTS_MAIN=$(BIN)/main.o $(BIN)/joystick.o $(BIN)gpio.o
OBJECTS_MAIN=$(BIN)/main.o \
			$(BIN)/audio.o \
			$(BIN)/controller.o \
			$(BIN)/mavlink.o \
			$(BIN)/gpio.o \
			$(BIN)/flight.o

.PHONY: all
all: init $(OBJECTS_MAIN)
		$(CC) $(CFLAGS) -o $(BIN)/$(BINARY) $(OBJECTS_MAIN) $(LDFLAGS)

.PHONY: init
init:
		@mkdir -p $(BIN)
		
.PHONY: clean
clean:
		rm -rf ./bin
		
### Similar to the above, if you want to add another source file add
### it to this list as well with the same format
$(BIN)/main.o: $(SRC)/main.c
		$(CC) $(CFLAGS) -c $(SRC)/main.c -o $(BIN)/main.o

$(BIN)/audio.o: $(SRC)/audio.c
		$(CC) $(CFLAGS) -c $(SRC)/audio.c -o $(BIN)/audio.o

$(BIN)/controller.o: $(SRC)/controller.c
		$(CC) $(CFLAGS) -c $(SRC)/controller.c -o $(BIN)/controller.o

$(BIN)/mavlink.o: $(SRC)/mavlink.c
		$(CC) $(CFLAGS) -c $(SRC)/mavlink.c -o $(BIN)/mavlink.o

$(BIN)/gpio.o: $(SRC)/gpio.c
		$(CC) $(CFLAGS) -c $(SRC)/gpio.c -o $(BIN)/gpio.o

$(BIN)/flight.o: $(SRC)/flight.c
		$(CC) $(CFLAGS) -c $(SRC)/flight.c -o $(BIN)/flight.o

